package edu.uptc.taller.gui;

import java.awt.Color;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Panel extends JPanel {
    
    private VentanaPrincipal ventana;
    private JLabel m1,m2,m3,estadom1,estadom2,estadom3, estado;
    private JScrollPane contFila;
    private JPanel fila;
    private JTextArea m1info, m2info, m3info;
    private JButton anadir;
    
    public Panel(VentanaPrincipal ventana){
	this.ventana = ventana;
	setLayout(null);
	inicializarComponentes();
	add(m1);
	add(m2);
	add(m3);
	JLabel a = new JLabel("Módulo A");
	a.setBounds(80, 20, 250, 20);
	JLabel b = new JLabel("Módulo B");
	b.setBounds(310, 20, 250, 20);
	JLabel c = new JLabel("Módulo C");
	c.setBounds(550, 20, 250, 20);
	add(a);
	add(b);
	add(c);
	estadom1 = new JLabel("    LIBRE"); 
	estadom1.setBounds(80, 220, 250, 20);
	add(estadom1);
	estadom2 = new JLabel("    LIBRE"); 
	estadom2.setBounds(310, 220, 250, 20);
	add(estadom2);
	estadom3 = new JLabel("    LIBRE"); 
	estadom3.setBounds(550, 220, 250, 20);
	add(estadom3);
	add(contFila);
    }
    
    private void inicializarComponentes(){
	m1 = new JLabel();
	m1.setIcon(new ImageIcon(getClass().getResource("/icons/deskman.png")));
	m2 = new JLabel();
	m2.setIcon(new ImageIcon(getClass().getResource("/icons/deskwoman.png")));
	m3 = new JLabel();
	m3.setIcon(new ImageIcon(getClass().getResource("/icons/deskwoman2.png")));
	m1.setBounds(20, 40, 250, 200);
	m2.setBounds(255, 40, 250, 200);
	m3.setBounds(492, 40, 250, 200);
	
	m1info = new JTextArea();
	m1info.setOpaque(false);
	m1info.setEditable(false);
	m1info.setBounds(20, 240, 250, 100);
	add(m1info);
	
	m2info = new JTextArea();
	m2info.setEditable(false);
	m2info.setOpaque(false);
	m2info.setBounds(255, 240, 250, 200);
	add(m2info);
	
	m3info = new JTextArea();
	m3info.setEditable(false);
	m3info.setOpaque(false);
	m3info.setBounds(492, 240, 250, 200);
	add(m3info);
	
	fila = new JPanel();
	fila.setLayout(new BoxLayout(fila, BoxLayout.Y_AXIS));
	fila.setBackground(Color.white);
	contFila = new JScrollPane(fila);
	contFila.setBounds(240, 320, 210, 320);
	estado = new JLabel();
	estado.setBounds(230, 130, 250, 342);
	add(estado);
	
	anadir = new JButton("Añadir a la cola");
	anadir.setBounds(268, 650, 150, 30);
	anadir.setFocusable(false);
	anadir.addActionListener(ventana.getEventos());
	anadir.setActionCommand(Eventos.ADD);

	add(anadir);
	
    }

    public JLabel getM1() {
        return m1;
    }

    public void setM1(JLabel m1) {
        this.m1 = m1;
    }

    public JLabel getM2() {
        return m2;
    }

    public void setM2(JLabel m2) {
        this.m2 = m2;
    }

    public JLabel getM3() {
        return m3;
    }

    public void setM3(JLabel m3) {
        this.m3 = m3;
    }

    public JLabel getEstadom1() {
        return estadom1;
    }

    public void setEstadom1(JLabel estadom1) {
        this.estadom1 = estadom1;
    }

    public JLabel getEstadom2() {
        return estadom2;
    }

    public void setEstadom2(JLabel estadom2) {
        this.estadom2 = estadom2;
    }

    public JLabel getEstadom3() {
        return estadom3;
    }

    public void setEstadom3(JLabel estadom3) {
        this.estadom3 = estadom3;
    }

    public JPanel getFila() {
        return fila;
    }

    public void setFila(JPanel fila) {
        this.fila = fila;
    }

    public JScrollPane getContFila() {
        return contFila;
    }

    public JTextArea getM1info() {
        return m1info;
    }

    public JTextArea getM2info() {
        return m2info;
    }

    public JTextArea getM3info() {
        return m3info;
    }

    public JLabel getEstado() {
        return estado;
    }
    
}
