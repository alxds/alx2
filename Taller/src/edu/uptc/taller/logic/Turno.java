package edu.uptc.taller.logic;

import java.util.Random;

public class Turno{
    
    private String cliente;
    private int turno;
    
    public Turno(int turno) {
	super();
	this.turno = turno;
	this.cliente = generarCliente();
    }
    
    private String generarCliente(){
	String cod = "";
	char[] elementos={'0','1','2','3','4','5','6','7','8','9' ,'a',
		'b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t',
		'u','v','w','x','y','z','A',
		'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T',
		'U','V','W','X','Y','Z'};
	
	for (int i = 0; i < 5; i++) {
	    Random rnd = new Random();
	    cod += elementos[rnd.nextInt(elementos.length)];
	    
	}
	
	return cod;
    }

    @Override
    public String toString() {
	return "Turno [cliente=" + cliente + ", turno=" + turno + "]";
    }

    public String getCliente() {
        return cliente;
    }

    public int getTurno() {
        return turno;
    }


    

}
