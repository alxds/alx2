package edu.uptc.taller.logic;

public class HiloTurno extends Thread{
    
    private Cola cola;
    private int ultimoTurno = 0; 

    public HiloTurno(Cola cola) {
	super();
	this.cola = cola;
    }
    
    @Override
    public void run() {
	if (cola.isEmpty()){
	    Turno turno = new Turno(ultimoTurno+1);
	        cola.add(turno);
	        ultimoTurno = ultimoTurno + 1;
	}
	else{
	    Nodo ultimo = null;
	    Nodo aux = cola.getHead();
	    while(aux != null){
		if (aux.getNext() == null){
		    ultimo = aux;
		}
		aux = aux.getNext();
	    }
	    Turno turno = new Turno(ultimo.getTurno().getTurno()+1);
	        cola.add(turno);
	        ultimoTurno = ultimo.getTurno().getTurno()+1;
	}
        
    }
}
